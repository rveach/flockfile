#! /usr/bin/env python

import os
import shutil
import uuid
from nose.tools import raises

from flockfile import FlockFile, FileNotLocked

class TestClass1:

    def setUp(self):
        """ set the test file name, nothing else. """

        # to ensure we have some randomness, let's use a uuid4
        self.test_file_name = str(uuid.uuid4())
        # then we'll make sure it doesn't exist.
        assert not os.path.exists(os.path.join("/tmp", self.test_file_name))

    def tearDown(self):
        """ in the weird event the test fails, clean up """

        test_file_fp = os.path.join("/tmp", self.test_file_name)

        if os.path.isfile(test_file_fp):
            os.remove(test_file_fp)

    def test_flock_file(self):
        """ a simple flock file test """

        # init the class
        lock = FlockFile(self.test_file_name)
        print("Using lock file {}.".format(lock.lock_file))

        # assert the file matches what it should.
        assert lock.lock_file == os.path.join("/tmp", self.test_file_name)
        # and that the file does not exist.
        assert not os.path.exists(lock.lock_file)
        assert not lock.check_lock()

        # lock it, assert our variable reflects this and that the file exists.
        lock.lock()
        assert os.path.isfile(lock.lock_file)
        assert lock.check_lock()

        # unlock check to make sure the file is absent
        lock.unlock()
        assert not os.path.exists(lock.lock_file)
        assert not lock.check_lock()


class TestClass2:

    def setUp(self):
        """ create a directory within tmp """

        self.test_dir = os.path.join("/tmp", str(uuid.uuid4()))
        os.mkdir(self.test_dir)

    def tearDown(self):
        """ remove our temp directory """

        shutil.rmtree(self.test_dir)

    @raises(FileNotLocked)
    def test_failed_lock(self):
        """ check concurrent locks on the same file """

        test_file_name = str(uuid.uuid4())

        lock_a = FlockFile(test_file_name, lock_dir=self.test_dir)
        lock_b = FlockFile(test_file_name, lock_dir=self.test_dir)

        lock_a.lock()
        assert lock_a.check_lock()
        assert not lock_b.check_lock()
        lock_b.lock()

class TestClass3:

    def setUp(self):
        """ set the test file name, nothing else. """

        # to ensure we have some randomness, let's use a uuid4
        self.test_file_name = str(uuid.uuid4())
        # then we'll make sure it doesn't exist.
        assert not os.path.exists(os.path.join("/tmp", self.test_file_name))

    def tearDown(self):
        """ This test should leave a file, but we want to clean it up. """

        test_file_fp = os.path.join("/tmp", self.test_file_name)

        if os.path.isfile(test_file_fp):
            os.remove(test_file_fp)

    def test_flock_file(self):
        """ test a lock file that won't get deleted """

        # init the class
        lock = FlockFile(self.test_file_name, delete_on_unlock=False)
        print("Using lock file {}.".format(lock.lock_file))

        # assert the file matches what it should.
        assert lock.lock_file == os.path.join("/tmp", self.test_file_name)
        # and that the file does not exist.
        assert not os.path.exists(lock.lock_file)
        assert not lock.check_lock()

        # lock it, assert our variable reflects this and that the file exists.
        lock.lock()
        assert os.path.isfile(lock.lock_file)
        assert lock.check_lock()

        # unlock check to make sure the file is absent
        lock.unlock()
        assert os.path.exists(lock.lock_file)
        assert not lock.check_lock()
